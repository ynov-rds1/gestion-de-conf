# I Enregistrer un nouveau client
➜ **Créer les fichiers de configuration du nouveau client**
- Copiez le modèle des fichiers `identity.tfvars` et `configuration.tfvars`
```bash
cp -r ./tp6/data/model ./tp6/data/client_name
```

➜ **Générer les credentials du nouveau client**
> *Azure CLI est requis sur votre poste pour cette étape.*
- Créez un nouvel abonnement dans votre tenant azure qui sera celui du client

- Remplacez les valeurs des variables **tenant_id** et **subscription_id** dans le fichier `./tp6/data/client_name/identity.tfvars` par l'identifiant de votre tenant et du nouvel abonnement

- Authentifiez-vous à Azure via `Azure cli`
```bash
az login
```
- Créez un principal de service ayant pour rôle Contributeur sur ce même abonnement
```bash
az ad sp create-for-rbac --name 'sp_client_name' --role 'Contributor' --scopes '/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'
```
- Remplacez les valeurs des variables **service_principal_id** et **service_principal_secret** dans le fichier `./tp6/data/client_name/identity.tfvars` par les credentials fournis dans l'output (**client_id** et **client_secret**, ou **appId** et **password**) de la commande précédente
```bash
# Exemple de contenu du fichier ./data/client_yjk/identity.tfvars
tenant_id = "fdsgfdsqfsd-dsgfds-zerfqs-1dfsq-f4c5sd4f6sd"
subscription_id = "fdsq4f5d6-4dr5-alv5-y7hd-1f4easg8t9d5"
service_principal_id = "r7d5c9zs-srf5-5cf6-8f2e-z7eedc7r5d6"
service_principal_name = "sp_client_a"
service_principal_secret = "aakci8f5-er7c-xr5f-7e4s-52c3deiksla9"
client_name = "client_a"
```
# II Définir la configuration du client
➜ **Générer la configuration du nouveau client**
- A partir du modèle fourni `./tp6/data/model/configuration.tfvars` et de la déclaration des variables pour les ressources disponibles (VM) à l'emplacement `./tp6/variables.tf`, rédigez une configuration pour le client

# III Monter la configuration du client
➜ **Voir le fichier ./tp6/execute.sh**

# IV Evolutions
- Automatiser la [première étape](#I-Enregistrer-un-nouveau-client) (De la génération des fichiers à la création d'abonnement etc...) : Récupérer données de config du client depuis une base de données dans job CI (artifacts identity.tfvars et configuration.tfvars) pour la gestion des données sensibles
- Rendre le fichier de configuration générique et compatible à plusieurs cloud providers
- Améliorer la structure des dossiers
- In-depth use of terraform plans for more detailed tasks