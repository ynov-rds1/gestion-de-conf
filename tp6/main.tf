terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
      configuration_aliases = [ azurerm.client ]
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

module "azure_setup_configuration" {
  source = "./modules//azure/setup_configuration"

  identity = var.client_name
  configuration = var.configuration

  providers = {
    azurerm = azurerm.client
  }
}

## TODO ##
# module "aws_setup_configuration" {
#   source = "./modules/aws/setup_configuration"

#   identity = var.client_name
#   configuration = var.configuration

#   providers = {
#     aws = aws.client
#   }
# }