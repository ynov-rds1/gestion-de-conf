#! /bin/sh

client=''
apply=false
destroy=false
auto_approve='-auto-approve';
provider='azure'

while [ $# -gt 0 ] ; do
  case $1 in
    -c | --client   ) client="$2" ;;
    -a | --apply    ) apply=true ;;
    -d | --destroy  ) destroy=true ;;
    -m | --manual   ) auto_approve='' ;;
    -p | --provider ) provider="$2"
  esac
  shift
done

identity_file="./data/$client/identity.tfvars"
configuration_file="./data/$client/configuration.tfvars"
target="module.${provider}_setup_configuration"

if [ ! -z "$client" ]
then
  echo "This script was written for testing purposes"
  echo "Selected provider is $provider"
  if $apply ; then
      echo "Applying configuration for $client..."
      terraform apply -target=$target -var-file="$identity_file" -var-file="$configuration_file" $auto_approve
  elif $destroy ; then
      echo "Destroying configuration for $client..."
      terraform destroy -target=$target -var-file="$identity_file" -var-file="$configuration_file" $auto_approve
  else 
      echo "Planning configuration for $client..."
      terraform plan -target=$target -var-file="$identity_file" -var-file="$configuration_file"
  fi
else
  echo "\$Client not set"
fi
