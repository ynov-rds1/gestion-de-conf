configuration = {
    resource_groups = {
        fr = {
            name = "rg-fr-a"
            location = "francecentral"
        }
        us = {
            name = "rg-us-a"
            location = "eastus"
        }
    }
    virtual_networks = {
        "vn_a" = {
            address_space = ["10.0.0.0/16"]
            resource_group = "fr"
        }
    }
    subnets = {
        "s_a" = {
            resource_group = "fr"
            virtual_network = "vn_a"
            address_prefixes = ["10.0.2.0/24"]
        }
    }
    network_interfaces = {
        "nic_a" = {
            resource_group = "fr"
            ip_config = {
                name = "internal"
                subnet = "s_a"
                private_ip_address_allocation = "Dynamic"
            }
        }
    }
    virtual_machines = {
        "vm-a-1" = {
            resource_group = "fr"                   
            subnet = "s_a"                          
            size = "Standard_B1s"                   
            admin_username = "client_admin_username"
            network_interface_ids = ["nic_a"]       
            admin_ssh_username = "rds"
            os_disk = {
                caching = "ReadWrite"
                storage_account_type = "Standard_LRS"
            }
            source_image_reference = {
                publisher = "Canonical"
                offer = "UbuntuServer"
                sku = "18.04-LTS"
                version = "latest"
            }
        }
    }
}
