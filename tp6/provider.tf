provider "azurerm" {
    features {}
}

provider "azurerm" {
    alias               = "client"
    tenant_id           = var.tenant_id
    subscription_id     = var.subscription_id
    client_id           = var.service_principal_id
    client_secret       = var.service_principal_secret
    features {}
}

# provider "aws" {
#     features{}
# }

# provider "aws" {
#   alias      = "client"
#   region     = var.region           # aws region
#   access_key = var.access_key       # aws access key
#   secret_key = var.secret_key       # aws secret key
# }