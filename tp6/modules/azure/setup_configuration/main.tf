resource "azurerm_resource_group" "rg_client" {
    for_each = var.configuration.resource_groups
    name      = each.value["name"]
    location  = each.value["location"]
}

resource "azurerm_virtual_network" "vn_client" {
  for_each = var.configuration.virtual_networks
  name                = each.key
  address_space       = each.value["address_space"]
  location            = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].location
  resource_group_name = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].name
}

resource "azurerm_subnet" "s_client" {
  for_each = var.configuration.subnets
  name                  = each.key
  resource_group_name   = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].name
  virtual_network_name  = azurerm_virtual_network.vn_client["${each.value["virtual_network"]}"].name
  address_prefixes      = each.value["address_prefixes"]
}

resource "azurerm_network_interface" "nic_client" {
  for_each = var.configuration.network_interfaces
  name                = each.key
  location            = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].location
  resource_group_name = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].name

  ip_configuration {
    name                          = each.value["ip_config"].name
    subnet_id                     = azurerm_subnet.s_client["${each.value["ip_config"].subnet}"].id
    private_ip_address_allocation = each.value["ip_config"].private_ip_address_allocation
  }
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "azurerm_linux_virtual_machine" "vm_client" {
  for_each = var.configuration.virtual_machines
  name                = each.key
  location            = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].location
  resource_group_name = azurerm_resource_group.rg_client["${each.value["resource_group"]}"].name
  size                = each.value["size"]            
  admin_username      = each.value["admin_username"]  
  network_interface_ids = [
    for nic in each.value["network_interface_ids"] : 
    azurerm_network_interface.nic_client["${nic}"].id
  ]

  admin_ssh_key {
    username   = each.value["admin_username"]
    public_key = tls_private_key.ssh.public_key_openssh
  }

  os_disk {
    caching              = each.value["os_disk"].caching
    storage_account_type = each.value["os_disk"].storage_account_type
  }

  source_image_reference {
    publisher = each.value["source_image_reference"].publisher
    offer     = each.value["source_image_reference"].offer
    sku       = each.value["source_image_reference"].sku
    version   = each.value["source_image_reference"].version
  }
}
