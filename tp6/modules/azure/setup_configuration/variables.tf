variable "identity" {
    type = string
}

variable "configuration" {
    type = object({
        resource_groups = optional(map(object({
            name = string
            location = string
        })))
        virtual_networks = optional(map(object( {   # key = virtual_network name
            address_space = list(string)
            resource_group = string                 # resource_group key (name)
        })))
        subnets = optional(map(object( {            # key = subnet name
            resource_group = string                 # resource_group key (name)
            virtual_network = string                # virtual_network key (name)
            address_prefixes = list(string) 
        })))
        network_interfaces = optional(map(object( { # key = network_interface name
            resource_group = string                 # resource_group key (name)
            ip_config = object({
                name = string
                subnet = string                     # subnet key (name)
                private_ip_address_allocation = string
            })
        })))
        virtual_machines = optional(map(object( {   # key = virtual_machine name
            resource_group = string                 # resource_group key (name)
            subnet = string                         # subnet key (name)
            network_interface_ids = list(string)    # network_interface keys (list of names)
            size = string
            admin_username = string                 
            os_disk = object({
                caching = string
                storage_account_type = string
            })
            source_image_reference = object({
                publisher = string
                offer = string
                sku = string
                version = string
            })
        })))
    })
}